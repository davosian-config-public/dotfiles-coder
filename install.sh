#!/bin/zsh

# see https://github.com/bpmct/dot for a coder dotfiles repo example

# Make zsh my default shell
# sudo apt-get install -y zsh
# sudo chsh -s $(readlink -f $(which zsh)) $USER

# Prepare home directory for prezto. Update with `zprezto-update`
git clone --recursive https://gitlab.com/davosian-config-public/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"

setopt EXTENDED_GLOB
for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
  ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
done

# copy dotfiles into ~
# For zsh we can use the following:
# The leading backslash instructs zsh to use the non-alias version of cp because otherwise it is set to prompt for overriding with `cp -i`
# (D) changes the glob settings to also include .dotfiles
# To test the selection use `print -rl`
\cp -rf ~/dotfiles/^(.git|.zshrc|.zprofile|.zshenv)(D) ~
# Bash: to include dotfiles in wildcards, we need to set options and make sure to exclude . and ..
# shopt -s dotglob # include . in *
# shopt -s extglob
# yes | cp -rf ~/dotfiles/!(.git|.zshrc|.zprofile|.zshenv|.|..) ~

# Append shell configuration stuff to end of their respective files
touch ~/.zshrc # not sure if this will always exist at this point :/
cat .zshrc >> ~/.zshrc

touch ~/.zprofile # not sure if this will always exist at this point :/
cat .zprofile >> ~/.zprofile

touch ~/.zshenv # not sure if this will always exist at this point :/
cat .zshenv >> ~/.zshenv
